#include "Client.h"
#include <QString>
/** \brief Constructeur
 *
 *
 */
Client::Client(int ident, string n, string lib, string comp, string vil, int code, string mail)
: identifiant(ident)
{
    this->Setnom(n);
    this->adPost.setInit(lib,comp,vil,code);
    this->Setmail(mail);
}
/** \brief Destructeur
 *
 *
 */
Client::~Client()
{
    //dtor
    if (comments)
    {
        cout<<"The client "<<this->Getnom()<<" have been removed "<<endl;
    }
}
/** \brief Setter du nom , verifie longeur
*/
void Client::Setnom(string val)
{
    if (val.length()>charLong)
    {
        throw InputException(ErrCode::ERR_CHAINE,"nom",charLong);
    }
    else
    {
        this->nom=val;
    }
}
/** \brief Setter du mail , verifie qu'il containt '@'
*/
void Client::Setmail(string val)
{
    if(val.find("@")!=string::npos)
    {
        this->mail=val;
    }
    else
    {
        throw InputException(ErrCode::ERR_MAIL);
    }
}
/** \brief Retour un string avec les informations generals du client
*
* \return string
*
*/
string Client::afficher()
{
    ostringstream oss;
    //oss<<"\t Identifiant : "<<this->Getidentifiant()<<endl;
    oss<<"\t Nom : "<<this->Getnom()<<endl;
    oss<<"\t Adresse Postale : "<<endl;
    oss<<this->GetadPost().afficher();
    oss<<"\t Mail de contact : "<<this->Getmail()<<endl;
    return oss.str();
}

string Client::toStrAdressScreen()
{
    ostringstream oss;
    oss<<this->tab()<<this->GetadPost().getLibelle()<<endl;
    // Formatage CP et Ville
    oss<<this->tab()<<this->GetadPost().getCP()<<" "<<QString::fromStdString(this->GetadPost().getVille()).toUpper().toStdString()<<endl;
    return oss.str();
}
