#include "listclients.h"
#include "ui_listclients.h"

ListClients::ListClients(QWidget *parent, QString txt) :
    QDialog(parent),
    ui(new Ui::ListClients)
{
    ui->setupUi(this);
    ui->brwsClients->setText(txt);
}

ListClients::~ListClients()
{
    delete ui;
}

void ListClients::on_btnCls_clicked()
{
    this->close();
}
