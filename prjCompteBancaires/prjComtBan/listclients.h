#ifndef LISTCLIENTS_H
#define LISTCLIENTS_H

#include <QDialog>
#include "InputException.h"
/*!
* \file listclients.h
* \brief Fichier dedie a la structure ListClients utilisé pour gerer les fenetres de List Clients et Importation des operations bancaires
* \author Pedro Clavier
* \version 3.0
*/

/*! \class ListClients
* \brief Fenetre qui affiche le QString passe dans ça creation.
*
*    Affiche le QString dans une nouvelle fenetre.
*    Originalement utilisé pour List Clients et plus tard utilisé aussi pour la Importation des Opérations bancaires
*/
namespace Ui {
class ListClients;
}

class ListClients : public QDialog
{
    Q_OBJECT

public:
    explicit ListClients(QWidget *parent = nullptr,QString txt="");
    ~ListClients();

private slots:
    void on_btnCls_clicked();

private:
    Ui::ListClients *ui;
};

#endif // LISTCLIENTS_H
