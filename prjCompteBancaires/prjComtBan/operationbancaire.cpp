#include "operationbancaire.h"

OperationBancaire::OperationBancaire(QString tit, int numCompt, float retrait, float depot, float carteB, float oldSolde, float newSolde, float decouvert, string msg)
{
    this->init(tit,numCompt,oldSolde,decouvert);
    this->setRetrait(retrait);
    this->setDepot(depot);
    this->setCarteB(carteB);
    this->setNewSolde(newSolde);
    this->setMsgDec(msg);
}

QString OperationBancaire::getTit() const
{
    return tit;
}

void OperationBancaire::setTit(const QString &value)
{
    tit = value;
}

int OperationBancaire::getNumCompt() const
{
    return numCompt;
}

void OperationBancaire::setNumCompt(int value)
{
    numCompt = value;
}

float OperationBancaire::getRetrait() const
{
    return retrait;
}

void OperationBancaire::setRetrait(float value)
{
    retrait = value;
}

float OperationBancaire::getDepot() const
{
    return depot;
}

void OperationBancaire::setDepot(float value)
{
    depot = value;
}

float OperationBancaire::getCarteB() const
{
    return carteB;
}

void OperationBancaire::setCarteB(float value)
{
    carteB = value;
}

float OperationBancaire::getOldSolde() const
{
    return oldSolde;
}

void OperationBancaire::setOldSolde(float value)
{
    oldSolde = value;
}

float OperationBancaire::getNewSolde() const
{
    return newSolde;
}

void OperationBancaire::setNewSolde(float value)
{
    newSolde = value;
}

float OperationBancaire::getDecouvert() const
{
    return decouvert;
}

void OperationBancaire::setDecouvert(float value)
{
    decouvert = value;
}

void OperationBancaire::init(QString titu,int nCmpt,float oldSolde,float dec)
{
    this->setTit(titu);
    this->setNumCompt(nCmpt);
    this->setRetrait(0);
    this->setDepot(0);
    this->setCarteB(0);
    this->setOldSolde(oldSolde);
    this->setNewSolde(oldSolde); // Il part avec l'ancien solde
    this->setDecouvert(dec);
    this->setMsgDec("");
}

string OperationBancaire::toStr()
{
    ostringstream oss;
    oss<<this->tab()<<"Titulaire : "<<(this->getTit()).toStdString()<<endl<<endl;
    oss<<this->tab()<<"Compte Numero : "<<this->getNumCompt()<<endl<<endl;
    oss<<this->tab()<<"     "<<"Total Retrait de "<<this->getRetrait()<<" €."<<endl;
    oss<<this->tab()<<"     "<<"Total Dépôt de "<<this->getDepot()<<" €."<<endl;
    oss<<this->tab()<<"     "<<"Total Carte Bleue de "<<this->getCarteB()<<" €."<<endl;
    oss<<endl<<endl;
    oss<<this->tab()<<"Solde avant mise à jour : "<<this->getOldSolde()<<endl;
    oss<<this->tab()<<"Solde apres mise à jour : "<<this->getNewSolde()<<endl;
    oss<<endl;
    if (this->getMsgDec().length()>0)
    {
        oss<<this->tab()<<"Attention : \n"<<this->getMsgDec()<<endl;
    }
    oss<<endl;
    return oss.str();
}

string OperationBancaire::getMsgDec() const
{
    return msgDec;
}

void OperationBancaire::setMsgDec(const string &value)
{
    msgDec = value;
}
