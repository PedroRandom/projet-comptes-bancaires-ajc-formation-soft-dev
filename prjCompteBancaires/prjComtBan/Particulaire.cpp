#include "Particulaire.h"
#include <QString>
#include <ctime>
Particulaire::Particulaire(int ident, string n, string lib, string comp, string vil, int code, string mail, int j, int m, int a, string pren, char sex)
: Client(ident,n,lib,comp,vil,code,mail)
{
    this->naissance.setInit(j,m,a);
    this->Setprenom(pren);
    this->Setsexe(sex);
}
/** \brief Setter du prenom , verifie longeur
*/
void Particulaire::Setprenom(string val)
{
    if (val.length()>charLong)
    {
        throw InputException(ErrCode::ERR_CHAINE,"prenom",charLong);
    }
    else
    {
        this->prenom=val;
    }
}
/** \brief Setter du sexe , verifie M/F
*/
void Particulaire::Setsexe(char val)
{
    if (val=='M'||val=='F')
    {
        this->sexe=val;
    }
    else
    {
        throw InputException(ErrCode::ERR_SEX);
    }
}
/** Return the string for age line in interface graphique
 * \return The age and "Bon anniversaire" if it is the birthday
 */
string Particulaire::GetAfficheAnniv()
{
    ostringstream oss;
    time_t t = time(0);
    tm* now = localtime(&t);
    oss<<1900+now->tm_year-this->Getnaissance().Getanne();
    if(now->tm_mon+1-this->Getnaissance().Getmois()==0 &&
       now->tm_mday-this->Getnaissance().Getjour()==0)
    {
        oss<<" et Bon Anniversaire !";
    }
    oss<<endl;
    return oss.str();
}
/** \brief Method utilis� pour l'affichage dans l'ecran (Seuelement une part est affiche) avec polymorphisme
*
* \return string
*
*/
string Particulaire::toStringScreen()
{
    ostringstream oss;
    oss<<"Particulier : "<<setfill('0')<<setw(5)<<this->Getidentifiant()<<endl<<endl;
    oss<<this->tab();//setfill(' ')<<setw(12);
    if(this->Getsexe()=='M')
    {
        oss<<"M. ";
    }
    else
    {
        oss<<"Mme ";
    }
    // Formatage nom et prenom
    oss<<QString::fromStdString(this->Getnom()).toUpper().toStdString()<<" "
       <<QString::fromStdString(this->Getprenom()).toUpper().toStdString()[0]
       <<QString::fromStdString(this->Getprenom().substr(1)).toLower().toStdString()<<endl;
    oss<<this->toStrAdressScreen();
    oss<<this->tab()<<"Age : "<<this->GetAfficheAnniv()<<endl;
    return oss.str();
}

/** \brief Method utilis� pour le polymorphisme, ajout les infos generales et les specifiques (celles du afficher)
*
* \return string
*
*/
string Particulaire::toString()
{
    ostringstream oss;
    oss<<"Particulaire : "<<setfill('0')<<setw(5)<<this->Getidentifiant()<<endl;
    oss<<Client::afficher()<<this->afficher();
    return oss.str();
}
/** \brief Retour un string avec les informations specifiques du client particulaire
*
* \return string
*
*/
string Particulaire::afficher()
{
    ostringstream oss;
    oss<<"\t Date the naissance : "<<this->Getnaissance().afficher();
    oss<<"\t Prenom : "<<this->Getprenom()<<endl;
    oss<<"\t Sexe : "<<this->Getsexe()<<endl;
    return oss.str();
}

/** \brief Destructeur
 *
 *
 */
Particulaire::~Particulaire()
{
    //dtor
    if (comments)
    {
        cout<<"The client particulaire "<<this->Getnom()<<" have been removed "<<endl;
    }

}
