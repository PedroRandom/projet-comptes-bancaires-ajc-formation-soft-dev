#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlDatabase> //Pour faire include de cette fichier il faut modifier le fichier .pro!!!
#include <QFileDialog>
#include "dataclients.h"
#include "Particulaire.h"
#include "Professionelle.h"
#include "InputException.h"
#include "listclients.h"
#include "lstcomptes.h"
#include "lstclientavance.h"
#include "operationbancaire.h"

/*!
* \file mainwindows.h
* \brief Fichier dedie a la structure MainWindow utilisé pour gerer la fenetre principale de l'application
* \author Pedro Clavier
* \version 3.0
*/

/*! \class Main Windows
* \brief Classe que sert de basse pour lancer la fenetre principale et les autres. Contient les donnes des clients et la database.
*
*    Elle permet de lancer les autres fenetres. C'est dans cette fenetre où tout la gestion de donnes est fait.
*       Getter/Setter
*       openDB
*       initDataBanque
*       updateDataBanque
*       lectureOperations
*       traitementOperations
*       initOperation
*       writeFile
*/

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    DataClients* getDataBanque() {return this->dataBanque;}
    void setDataBanque(DataClients* val){this->dataBanque=val;}
    /** \brief Permet d'ouvrir la database
    *
    * \return Void
    *
    */
    void openDB();
    /** \brief Permet d'initialiser la class dataBanque avec les donnees des clients fournis
    *
    * \return Void
    *
    */
    void initDataBanque();
    /** \brief Permet de mettre a jour la data base des comptes avec les operations effectues
    *
    * \return Void
    *
    */
    void updateDataBanque(OperationBancaire);
    /** \brief Lance une fenetre pour choisir le fichier Operations.txt (car c'est un fichier change regulierment) et retour un QString avec son contenue
    *
    * \return QString
    *
    */
    QString lectureOperations();
    /** \brief Traite les donnes extraites du fichier, applique les changements dans la db et retour le QString a afficher dans la nouvelle ecran
    *
    * \return QString
    *
    */
    QString traitementOperations(QString);

    void initOperation(OperationBancaire &,int numCompte);

    /** \brief Permet d'ecrire le QString dans un fichier
    *
    * \return void
    *
    */
    void writeFile(QString,string);

private slots:
    void on_btnListClient_clicked(); // Option List Clients (si plus de temps mettre un nom plus representative)

    void on_btnConsCompt_clicked(); // Option Consultation Comptes (si plus de temps mettre un nom plus representative)

    void on_btnOps_clicked(); // Option Importation des operations bancaires (si plus de temps mettre un nom plus representative)

    void on_actionSortir_triggered(); // Fermer la fenetre

    void on_actionChanger_DB_triggered();   //Permet ouvrir l'explorateur pour changer la DB

    void on_lstClientsAv_clicked();

private:
    Ui::MainWindow *ui; /**< Interface graphique */
    DataClients* dataBanque; /**< Structure avec les donnees des clients */
    QSqlDatabase db; /**< Database avec les comptes clients */
};

#endif // MAINWINDOW_H
