#include "lstcomptes.h"
#include "ui_lstcomptes.h"

lstComptes::lstComptes(QWidget *parent, QSqlDatabase db) :
    QDialog(parent),
    ui(new Ui::lstComptes)
{
    ui->setupUi(this);
    this->db=db;
    this->init();
}

lstComptes::~lstComptes()
{
    delete ui;
}

void lstComptes::init()
{
    QSqlQueryModel *dbCompt = new QSqlQueryModel();
    QString reqSqlCompt = "select * from comptes;";//numcompte,datecreation,solde,decouvert,numcli from comptes;";

    if (this->db.open()) // Verification du db
    {
        if (comments)
        {
            cout<<" Open DB ok"<<endl;
        }
    }
    else {
        throw InputException(ErrCode::ERR_DB);
    }
    dbCompt->setQuery(reqSqlCompt,this->db);

    dbCompt->setHeaderData(0,Qt::Horizontal,"Numero de Compte");
    dbCompt->setHeaderData(1,Qt::Horizontal,"Date de Creation");
    dbCompt->setHeaderData(2,Qt::Horizontal,"Solde");
    dbCompt->setHeaderData(3,Qt::Horizontal,"Decouvert");
    dbCompt->setHeaderData(4,Qt::Horizontal,"Numero du Client");

    ui->tblCmpts->setModel(dbCompt);
    //Imposer la selection d'une seule ligne
    ui->tblCmpts->setSelectionMode(QAbstractItemView::SingleSelection);
    //Selection de la ligne complet
    ui->tblCmpts->setSelectionBehavior(QAbstractItemView::SelectRows);
    this->db.close();
}

void lstComptes::on_pushButton_clicked()
{
    this->close();
}
