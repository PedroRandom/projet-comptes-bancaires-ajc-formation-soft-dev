#ifndef OPERATIONBANCAIRE_H
#define OPERATIONBANCAIRE_H
#include <QString>
#include "InputException.h"
/*!
* \file operationbancaire.h
* \brief Fichier dedie a la structure OperationBancaire utilisé pour gerer les operations bancaires
* \author Pedro Clavier
* \version 2.0
*/

/*! \class OperationBancaire
* \brief Classe qui contient les donnes des Operations Bancaires liées au même compte
*
*    Elle permet de gérer les donnes d'operations et generer le resume pour l'affichage
*       Getter/Setter
*       afficher
*/
class OperationBancaire
{
public:
    OperationBancaire(QString="A remplir",int=-1,float=0,float=0,float=0,float=0,float=0,float=0,string="");
    QString getTit() const;
    void setTit(const QString &value);

    int getNumCompt() const;
    void setNumCompt(int value);

    float getRetrait() const;
    void setRetrait(float value);

    float getDepot() const;
    void setDepot(float value);

    float getCarteB() const;
    void setCarteB(float value);

    float getOldSolde() const;
    void setOldSolde(float value);

    float getNewSolde() const;
    void setNewSolde(float value);

    float getDecouvert() const;
    void setDecouvert(float value);

    string getMsgDec() const;
    void setMsgDec(const string &value);

    /** \brief Sert a initialiser tous les donnes de la classe
    *
    * \return void
    *
    */
    void init(QString titu="A remplir",int nCmpt=-1,float oldSolde=0,float dec=0);
    /** \brief Produis le message de l'operation que sera affiche par l'ecran
    *
    * \return string
    *
    */
    string toStr();

    /** \brief Method utilis� pour creer des tabulations pour l'affichage dans l'ecran
    *
    * \return ostringstream
    *
    */
    string tab()
    {
        ostringstream oss;
        oss<<setfill(' ')<<setw(tabLong)<<" ";
        return oss.str();
    }
    /** \brief Operator + , utilisé pour les depots dans le compte
    *
    * \return void
    *
    */
    void operator+ (float depot)
    {
        this->setDepot(this->getDepot()+depot);
        this->setNewSolde(this->getNewSolde()+depot);
    }
    /** \brief Operator + , utilisé pour les retraits dans le compte
    *
    * \return void
    *
    */
    void operator- (float retrait)
    {
        this->setRetrait(this->getRetrait()+retrait);
        this->setNewSolde(this->getNewSolde()-retrait);
        if (this->getNewSolde()<this->getDecouvert())
        {
            this->setMsgDec(this->getMsgDec()+this->tab()+"     "+"Compte en decouvert!!! \n");
        }
    }
    /** \brief Operator * , utilisé pour les operations avec carte blue
    *
    * \return void
    *
    */
    void operator* (float cb)
    {
        this->setCarteB(this->getCarteB()+cb);
        this->setNewSolde(this->getNewSolde()-cb);
        if (this->getNewSolde()<this->getDecouvert())
        {
            this->setMsgDec(this->getMsgDec()+this->tab()+"     "+"Compte en decouvert!!! \n");
        }
    }

private:
    QString tit;     /**< Titulaire du compte */
    int numCompt;    /**< Numero du compte */
    float retrait;   /**< Total retiré du compte */
    float depot;     /**< Total deposite dans le compte */
    float carteB;    /**< Total depense avec carte blue */
    float oldSolde;  /**< Ancien solde du compte */
    float newSolde;  /**< Nouvelle solde du compte */
    float decouvert; /**< Decouvert autorisé du compte */
    string msgDec; /**< Decouvert autorisé du compte */
};

#endif // OPERATIONBANCAIRE_H
