#ifndef DATACLIENTS_H
#define DATACLIENTS_H

#include "Client.h"

/*!
* \file dataClients.h
* \brief Fichier dedie a gerer les donnes de tous les Clients
* \author Pedro Clavier
* \version 0.6
*/

/*! \class DataClients
* \brief classe dedie a soporter les infos de tous les clients
*
*    Elle permet de gerer les infos de tous les clients
*       Getter/Setters
*       nbClients
*       affiche
*       afficheSimp
*       operator[int]
*/
class DataClients
{
public:
    /** Default constructor */
    DataClients();
    /** Default destructor */
    virtual ~DataClients();
    /** Access lstClients
     * \return The current list of clients
     */
    map<int,Client*> GetlstClients() { return lstClients; }
    void SetlstClients(map<int,Client*> val) { lstClients = val; }
    /** \brief Additionne un nouvelle client à la liste, s'il existe dejà il retourne un erreur
    *
    * \return void
    *
    */
    void addClient(Client* val);
    /** \brief Elimine le client de la list qui est positione a val
    *
    * \return void
    *
    */
    void sup(int val){
        delete lstClients.at(val); // 1er On elimine le pointeur
        lstClients.erase(val);} // 2eme On elimine le celle de la list en utilisant la key
    /** \brief Retour le numero de Clients dans le vecteur lstClients
    *
    * \return int
    *
    */
    int nbClients(){return lstClients.size();}
    /** \brief Retour un string avec la liste de tous les clients et son information
    *
    * \return string
    *
    */
    string affiche()
    {
        ostringstream oss;
        //oss<<"La information complete de nos "<<this->nbClients()<<" clients est :"<<endl;
        //oss<<"--------------------------------"<<endl;
        for (auto val: this->GetlstClients())
        {
            oss<<val.second->toStringScreen()<<"‎"; // Addition d'un empty char pour marquer le fin d'un client sans modifier l'affichage (util pour l'split en list)
            //oss<<"--------------------------------"<<endl;
        }
        return oss.str();
    }
    /** \brief Retour un string avec la liste des noms de clients
    *
    * \return string
    *
    */
    void afficheSimp()
    {
        cout<<"La liste de nos "<<this->nbClients()<<" clients est :"<<endl;
        for (auto val: this->GetlstClients())
        {
            cout<<"Mr/Ms "<<val.second->Getnom()<<endl;
        }
    }

private:
    map<int,Client*> lstClients; /**< Map contenant les infos des clients */
};

#endif // DATACLIENTS_H
