#include "InputException.h"
/** \brief Constructeur
 *
 *
 */
InputException::InputException(ErrCode cod, string t, int maxim)
: code(cod),text(t),maxi(maxim)
{
}

/** \brief Destructeur
 *
 *
 */
InputException::~InputException()
{
    //dtor
    if(comments)
    {
        cout<<"Destruction du exception "<<endl;
    }
}

/** \brief Retour un string avec les informations de l'erreur
*
* \return string
*
*/
const char* InputException::what() const throw()
{
    ostringstream oss;
    string message;
    switch(this->code)
    {
        case ErrCode::ERR_CP:
            oss<<"Le "<<this->text<<" introduit doit etre positif."<<endl;
            break;
        case ErrCode::ERR_JOUR:
            oss<<"Le jour saissi doit etre entre 1 et 31 ."<<endl;
            break;
        case ErrCode::ERR_MOIS:
            oss<<"Le mois saissi doit etre entre 1 et 12 ."<<endl;
            break;
        case ErrCode::ERR_ANNE:
            oss<<"L'anne saissi doit etre en fomat de 4 chifres (AAAA) ."<<endl;
            break;
        case ErrCode::ERR_MAIL:
            oss<<"Un mail doit etre contenir le character '@' ."<<endl;
            break;
        case ErrCode::ERR_CHAINE:
            oss<<"Le "<<this->text<<" doit avoir moins de "<<this->maxi<<" characters ."<<endl;
            break;
        case ErrCode::ERR_SEX:
            oss<<"Le sexe doit etre M/F "<<endl;
            break;
        case ErrCode::ERR_NUM_DET:
            oss<<"Le "<<this->text<<" doit etre un numero de "<<this->maxi<<" chiffres ."<<endl;
            break;
        case ErrCode::ERR_ST_JUR:
            oss<<"Le status juridique doit �tre SARL, SA, SAS ou EURL"<<endl;
            break;
        case ErrCode::ERR_MAX:
            oss<<"L'index introduit doit etre inferieur ou egal a "<<this->maxi<<" ."<<endl;
            break;
        case ErrCode::ERR_CL_EXIST:
            oss<<"L'identifiant introduit pour le nouvelle client existe deja dans la database, verifiez l'identifiant"<<endl;
            break;
        case ErrCode::ERR_DB:
            oss<<"Database inaccesible, une fenetre s'ouvrira dans le directoire par defaut pour que vous choisisez la data base a ouvrir ";
            break;
        case ErrCode::ERR_FIC:
            oss<<"Erreur avec l'ouverture du fichier "<<this->text<<", verifiez que le fichier est accesible "<<endl;
            break;
        default:
            oss<<"Erreur non gere "<<endl;
            break;
    }
    this->expErr=oss.str(); // Il faut le garder dans l'objet parce que si non, la explication serait detruit avant de pouvoir etre lis au main
    return this->expErr.c_str();
}

