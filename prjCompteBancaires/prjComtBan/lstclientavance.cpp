#include "lstclientavance.h"
#include "ui_lstclientavance.h"
#include "QDebug"

#include <QAbstractItemView>
#include <QTableWidget>

lstClientAvance::lstClientAvance(QWidget *parent,DataClients* data) :
    QDialog(parent),
    ui(new Ui::lstClientAvance)
{
    ui->setupUi(this);
    this->setData(data);
    int index=0;
    ui->tblClients->setColumnCount(2);
    ui->tblClients->setRowCount(this->data->nbClients());
    for(auto val: this->data->GetlstClients())
    {
        ui->tblClients->setItem(index,0,new QTableWidgetItem(QString("%1").arg(val.first)));// 1er Col = nCompt
        ui->tblClients->setItem(index,1,new QTableWidgetItem(QString::fromStdString(val.second->toStringScreen()))); // 2eme Col = Text
        ui->tblClients->setRowHeight(index,200); // Malheuresement le resize to contents ne marche pas bien
        index++;
    }
    ui->tblClients->hideColumn(0);
    ui->tblClients->horizontalHeader()->hide();
    ui->tblClients->verticalHeader()->hide();
    ui->tblClients->setColumnWidth(1,700); // Malheuressement resize to contents ne laisse pas l'effet desiré
    ui->tblClients->setShowGrid(false);
    ui->tblClients->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

lstClientAvance::~lstClientAvance()
{
    delete ui;
}

void lstClientAvance::on_btnClose_clicked()
{
    this->close();
}

DataClients *lstClientAvance::getData() const
{
    return data;
}

void lstClientAvance::setData(DataClients *value)
{
    data = value;
}


void lstClientAvance::on_tblClients_itemDoubleClicked(QTableWidgetItem *item)
{
    QMessageBox msgbox;
    msgbox.setText(QString::fromStdString(data->GetlstClients()[ui->tblClients->item(item->row(),0)->text().toInt()]->toString()));
    msgbox.exec();
}
