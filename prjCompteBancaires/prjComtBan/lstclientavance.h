#ifndef LSTCLIENTAVANCE_H
#define LSTCLIENTAVANCE_H

#include <QDialog>
#include <QListWidget>
#include <QTableWidget>
#include <QMessageBox>
#include "dataclients.h"

namespace Ui {
class lstClientAvance;
}

class lstClientAvance : public QDialog
{
    Q_OBJECT

public:
    explicit lstClientAvance(QWidget *parent, DataClients*);
    ~lstClientAvance();

    DataClients *getData() const;
    void setData(DataClients *value);

private slots:
    void on_btnClose_clicked();



    void on_tblClients_itemDoubleClicked(QTableWidgetItem *item);

private:
    Ui::lstClientAvance *ui;
    DataClients* data;
};

#endif // LSTCLIENTAVANCE_H
