#ifndef PROFESSIONELLE_H
#define PROFESSIONELLE_H

#include "Client.h"
/*!
* \file Professionel.h
* \brief Fichier dedie a la structure concrete Professionels
* \author Pedro Clavier
* \version 0.4
*/

/*! \class stJur
* \brief Enumeration avec les diferents status Juridiques
*
*/
enum class stJur{SARL,SA,SAS,EURL};

/*! \class Proffesionelle
* \brief classe dedie a les infos specifiques des clients professionelles
*
*    Elle permet de g?rer les infos basse des clients professionelles
*       Getter/Setters
*       toString
*       afficher
*/
class Professionelle : public Client
{
    public:
        /** Default constructor */
        Professionelle(int ident=0,string n="A remplir",string lib="A reemplir",string comp="",string vil=" A reemplir",int code=0,string mail="aremplir@inconue.com",
                       string siret="00000000000000",stJur statu=stJur::SA,string libSiege="A reemplir", string compSiege="",string vilSiege="A reemplir",int codeSiege=0);
        /** Default destructor */
        virtual ~Professionelle();

        /** Access siret
         * \return The current value of siret
         */
        string Getsiret() { return siret; }
        /** Set siret , s'assure qu'il a au moins 14 chifres
         * \param val New value to set
         */
        void Setsiret(string val);
        /** Access status
         * \return The current value of status
         */
        stJur Getstatus() { return status; }
        /** Access status juridique en string
         * \return A stirng with the current value of status
         */
        string GetstatusStr();
        /** Set status
         * \param val New value to set
         */
        void Setstatus(stJur val) { status = val; } // Erreurs geres a niveau d'interface graphique!
        /** Access adrSiege
         * \return The current value of adrSiege
         */
        AdressePost GetadrSiege() { return adrSiege; }
        /** Set adrSiege
         * \param val New value to set
         */
        void SetadrSiege(AdressePost val) { adrSiege = val; }
        /** \brief Method utilis� pour l'affichage dans l'ecran (Seuelement une part est affiche) avec polymorphisme
        *
        * \return string
        *
        */
        virtual string toStringScreen() override;
        /** \brief Method utilis� pour le polymorphisme, ajout les infos generales et les specifiques (celles du afficher)
        *
        * \return string
        *
        */
        virtual string toString() override;
        /** \brief Retour un string avec les informations specifiques du client particulaire
        *
        * \return string
        *
        */
        virtual string afficher() override;

    protected:

    private:
        string siret; //!< SIRET du client prof
        stJur status; //!< Status juridique
        AdressePost adrSiege; //!< Adresse Postale de la siege
};

#endif // PROFESSIONELLE_H
