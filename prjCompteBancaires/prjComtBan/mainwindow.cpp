#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QResource>
#include <QDebug>
#include <QSqlQuery>
#include <QMessageBox>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    this->setDataBanque(new DataClients());
    this->initDataBanque();
    ui->setupUi(this);
    this->setWindowTitle("Gestion de comptes bancaires");
    this->db = QSqlDatabase::addDatabase("QSQLITE");
    this->db.setDatabaseName(QDir::currentPath()+"/../prjComtBan/BdComptes.db"); // Si plus de temps, mettre le paht a la dB dans un fichier .init!
    //Check du database
    this->openDB();
    this->db.close();
}

MainWindow::~MainWindow()
{
    delete this->getDataBanque();
    delete ui;
}

void MainWindow::openDB()
{
    bool ok=false;
    while(!ok)
    {
        try {
            if (this->db.open()) // Verification du db
            {
                if (comments)
                {
                    cout<<" Open DB ok"<<endl;
                }
                ok=true;
            }
            else {
                throw InputException(ErrCode::ERR_DB);
            }
        } catch (InputException &excp) {
            QMessageBox msgbox;
            msgbox.setText(excp.what());
            msgbox.exec();
            this->on_actionChanger_DB_triggered();
        }
    }
}

void MainWindow::initDataBanque()
{
    this->getDataBanque()->addClient(new Particulaire(1,"BETY","12, rue des Oliviers","","CRETEIL",94000,"bety@gmail.com",15,12,1985,"Daniel",'M'));
    this->getDataBanque()->addClient(new Professionelle(2,"AXA","125, rue LaFayette","Digicode 1432","FONTENAY SOUS BOIS",94120,"info@axa.fr","12548795641122",stJur::SARL,"125, rue LaFayette","Digicode 1432","FONTENAY SOUS BOIS",94120));
    this->getDataBanque()->addClient(new Particulaire(3,"BODIN","10,rue des Olivies","Etage 2","VINCENNES",94300,"boding@gmail.com",05,05,1965,"Justin",'M'));
    this->getDataBanque()->addClient(new Professionelle(4,"PAUL","36, quoi des Orfèbvres","","ROISSY EN France",93500,"info@paul.fr","87459564455444",stJur::EURL,"10, esplanade de la Défense","","LA DEFENSE",92060));
    this->getDataBanque()->addClient(new Particulaire(5,"BERRIS","15,rue de la République","","FONTENAY SOUS BOIS",94120,"berris@gmail.com",06,06,1977,"Karine",'F'));
    this->getDataBanque()->addClient(new Professionelle(6,"PRIMARK","32, rue E. Renan","Bat. C","PARIS",75002,"contact@primark.fr","08755897458455",stJur::SARL,"32, rue E. Renan","Bat C","PARIS",75002));
    this->getDataBanque()->addClient(new Particulaire(7,"ABENIR","25,rue de la Paix","","LA DEFENSE",92100,"abenir@gmail.com",12,04,1977,"Alexandra",'F'));
    this->getDataBanque()->addClient(new Professionelle(8,"ZARA","23, av P. Valery","","LA DEFENSE",92100,"info@zara.fr","65895874587854",stJur::SA,"24, esplanade de la Défense","Tour Franklin","LA DEFENSE",92060));
    this->getDataBanque()->addClient(new Particulaire(9,"BENSAID","3,avenue des Parcs","","ROISSY EN FRANCE",93500,"bensaid@gmail.com",16,04,1976,"Georgia",'F'));
    this->getDataBanque()->addClient(new Professionelle(10,"LEONIDAS","15, Place de la Bastille","FOND de Cour","PARIS",75003,"contact@leonidas.fr","91235987456832",stJur::SAS,"10, rue de la Paix","","PARIS",75008));
    this->getDataBanque()->addClient(new Particulaire(11,"ABADOU","3,rue Lecourbe","","BAGNOLET",93200,"abadou@gmail.com",10,10,1970,"Teddy",'M'));
}

void MainWindow::updateDataBanque(OperationBancaire op)
{
    // Mis a jour du nouvelle solde dans la db

    QSqlQuery query(db);
    query.prepare("UPDATE comptes SET solde = :newsolde where numcompte=:nCmpt;");
    query.bindValue(":newsolde",op.getNewSolde());
    query.bindValue(":nCmpt",op.getNumCompt());
    bool resQuery=query.exec();
    if (!resQuery)
    {
        QMessageBox msgbox;
        msgbox.setText("Ejec de mis a jour de la db");
        msgbox.exec();
    }
}



void MainWindow::on_btnListClient_clicked()
{
    ListClients*  myClients = new ListClients(this,QString::fromStdString(this->getDataBanque()->affiche()));
    myClients->setWindowTitle("List Clients");
    myClients->show();
}

void MainWindow::on_btnConsCompt_clicked()
{
    lstComptes* myComptes = new lstComptes(this,db) ;
    myComptes->setWindowTitle("List comptes");
    myComptes->show();
}

void MainWindow::on_btnOps_clicked()
{
    QString contenuFichier=this->lectureOperations();
    QString resOp = this->traitementOperations(contenuFichier);
    ListClients* myOp = new ListClients(this,resOp);
    myOp->setWindowTitle("List Operations");
    myOp->show();

}
QString MainWindow::lectureOperations()
{
    QString filename = QFileDialog::getOpenFileName(this,
                                                    QString("Selectionne le fichier des operations bancaires "),
                                                    QDir::currentPath()+"../../",
                                                    "Text files(*.txt)");
    QFile file(filename);
    bool openOk = file.open(QIODevice::ReadOnly|QIODevice::Text);
    QString contenuFichier;

    if (openOk)
    {
        contenuFichier = file.readAll();
    }
    else {
        throw InputException(ErrCode::ERR_FIC,filename.toStdString());
    }
    file.close();
    return contenuFichier;
}

QString MainWindow::traitementOperations(QString contFic)
{
    QString resOp="";
    QString anomalies="";
    OperationBancaire op;
    int numCompte=-1;
    int codeOp = -1;
    float montant= -1;
    this->openDB();
    for(QString line: contFic.split("\n"))
    {
        if (line!="")
        {
            numCompte = line.split(";")[0].toInt();
            codeOp = line.split(";")[2].toInt();
            montant = line.split(";")[3].toFloat();

            if (numCompte!=op.getNumCompt())
            {
                if (op.getNumCompt()!=-1)
                {
                    //Mis a jour du resOp
                    resOp+=QString::fromStdString(op.toStr());
                    //Mis a jour du base donnes (A faire au final apres verif du fonctionement)
                    this->updateDataBanque(op);
                }
                // Nouvelle Compte, il est mis a jour
                this->initOperation(op,numCompte);
            }
            // Inclusion de l'operation dans op!!
            switch(codeOp)
            {
                case 1: // Retrait DAB
                    op-montant;
                    break;
                case 2: // Paiement Carte Bleue
                    op*montant;
                    break;
                case 3: // Depot Guichet
                    op+montant;
                    break;
                default: // Anomalies.log
                    anomalies+=line+"\n";
                    break;
            }
        }
    }
    resOp+=QString::fromStdString(op.toStr()); // Inclusion du dernier set d'operations
    //Mis a jour du base donnes (A faire au final apres verif du fonctionement)
    this->updateDataBanque(op);
    //Si necessaire, ecriture du fichier anomalies
    this->writeFile(anomalies,anomFilename);
    this->db.close(); // On ferme la db
    return resOp;
}

void MainWindow::initOperation(OperationBancaire &op, int numCompte)
{
    QSqlQuery requeteSQL(this->db);
    QString titulaire = "";
    float oldSolde = -1.0;
    float decouvert = -1.0;
    int numClient = -1.0;

    requeteSQL.prepare("select solde,decouvert,numcli from comptes where numcompte=:nCmpt ;");
    requeteSQL.bindValue(":nCmpt",numCompte);

    bool resQuery=requeteSQL.exec();
    requeteSQL.next();
    if (resQuery)
    {
        oldSolde=requeteSQL.value("solde").toFloat();
        decouvert=requeteSQL.value("decouvert").toFloat();
        numClient=requeteSQL.value("numcli").toInt();
    }
    else {
        cout<<"Pas trouve"<<endl;
    }
    // Pour le titulaire on prend la deuxieme ligne de l'affichage
    titulaire=QString::fromStdString(this->getDataBanque()->GetlstClients()[numClient]->toStringScreen()).split("\n")[2];
    // On prend la part apres la tabulation
    // (de cette façon on a le formatage dejà fait)
    titulaire=titulaire.right(titulaire.length()-tabLong);
    op.init(titulaire,numCompte,oldSolde,decouvert);

}
void MainWindow::writeFile(QString content,string fName)
{
    if (content.length()>0)
    { // Si necessaire, ecriture du fichier
        QString filename = QDir::currentPath()+"../../"+QString::fromStdString(fName);
        QFile file(filename);
        bool openOk = file.open(QIODevice::WriteOnly|QIODevice::Text);
        if (openOk)
        {
            QTextStream stream(&file);
            stream<<content;

        }
        else {
            throw InputException(ErrCode::ERR_FIC,filename.toStdString());
        }
        file.close();
    }
}

void MainWindow::on_actionSortir_triggered()
{
    this->close();
}

void MainWindow::on_actionChanger_DB_triggered()
{
    this->db.close(); // Pour s'assurer
    QString dbName = QFileDialog::getOpenFileName(this,
                                                    QString("Selectionne la data base à utiliser "),
                                                    QDir::currentPath()+"/../prjComtBan/",
                                                    "Data base(*.db)");
    this->db.setDatabaseName(dbName);
    //Check du database
    this->openDB();
    this->db.close();
}


void MainWindow::on_lstClientsAv_clicked()
{
    lstClientAvance*  myClientsAv = new lstClientAvance(this,this->getDataBanque());
    myClientsAv->setWindowTitle("List Clients");
    myClientsAv->show();
}
