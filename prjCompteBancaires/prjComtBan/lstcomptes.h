#ifndef LSTCOMPTES_H
#define LSTCOMPTES_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlTableModel>
#include "InputException.h"
/*!
* \file listcomptes.h
* \brief Fichier dedie a la structure lstComptes utilisé pour gerer la fenetre de Consultation des Comptes
* \author Pedro Clavier
* \version 2.0
*/

/*! \class lstComptes
* \brief Class qui recoit la db, la ouvre et l'affiche dans une nouvelle fenetre.
*
*    Ouvre la db reçoit du Main Window, l'affiche dans la nouvelle fenetre et la ferme.
*/
namespace Ui {
class lstComptes;
}

class lstComptes : public QDialog
{
    Q_OBJECT

public:
    explicit lstComptes(QWidget *parent, QSqlDatabase db);
    ~lstComptes();

    /*Fonction utilisé pour produir l'affichage de la db dans l'ecran*/
    // return void
    void init();

private slots:
    void on_pushButton_clicked();

private:
    Ui::lstComptes *ui;
    QSqlDatabase db; /**< Database avec les comptes clients reçu du Main Window*/
};

#endif // LSTCOMPTES_H
