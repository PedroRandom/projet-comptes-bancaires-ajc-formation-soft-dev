#include "Professionelle.h"
#include <QRegExp>
#include <QString>

Professionelle::Professionelle(int ident, string n, string lib, string comp, string vil, int code, string mail, string siret, stJur statu, string libSiege, string compSiege, string vilSiege, int codeSiege)
: Client(ident,n,lib,comp,vil,code,mail)
{
    this->Setsiret(siret);
    this->Setstatus(statu);
    this->adrSiege.setInit(libSiege,compSiege,vilSiege,codeSiege);
}

void Professionelle::Setsiret(string val)
{
    QRegExp num(QString("^[0-9]{%1}$").arg(sirLong));
    if (!num.exactMatch(QString::fromStdString(val)))//(val.length()!=sirLong) // A faire la parte numerique avec QT et les expr regulieres!!!!
    {
        throw InputException(ErrCode::ERR_NUM_DET,"siret",sirLong);
    }
    else
    {
        this->siret=val;
    }
}
/** Access status juridique en string
* \return A stirng with the current value of status
*/
string Professionelle::GetstatusStr()
{
    switch (this->Getstatus())
    {
    case stJur::EURL:
        return "EURL";
    case stJur::SA:
        return "SA";
    case stJur::SARL:
        return "SARL";
    case stJur::SAS:
        return "SAS";
    default:
        throw InputException(ErrCode::ERR_ST_JUR); // A reviser dans le future en fonction de gestion des fichiers/base donnes!
    }
}
/** \brief Method utilis� pour l'affichage dans l'ecran (Seuelement une part est affiche) avec polymorphisme
*
* \return string
*
*/
string Professionelle::toStringScreen()
{
    ostringstream oss;
    oss<<"Professionelle : "<<setfill('0')<<setw(5)<<this->Getidentifiant()<<endl<<endl;
    oss<<this->tab()<<"Siret : "<<this->Getsiret()<<endl;
    oss<<this->tab()<<this->GetstatusStr()<<" "<<QString::fromStdString(this->Getnom()).toUpper().toStdString()<<endl;
    oss<<this->toStrAdressScreen()<<endl;
    oss<<this->tab()<<"Mail : "<<this->Getmail()<<endl<<endl;
    return oss.str();
}


/** \brief Method utilis� pour le polymorphisme, ajout les infos generales et les specifiques (celles du afficher)
*
* \return string
*
*/
string Professionelle::toString()
{
    ostringstream oss;
    oss<<"Professionelle : "<<setfill('0')<<setw(5)<<this->Getidentifiant()<<endl;
    oss<<Client::afficher()<<this->afficher();
    return oss.str();
}

/** \brief Retour un string avec les informations specifiques du client particulaire
*
* \return string
*
*/
string Professionelle::afficher()
{
    ostringstream oss;
    oss<<"\t Siret : "<<this->Getsiret()<<endl;;
    oss<<"\t Statut Juridique : "<<this->GetstatusStr()<<endl;
    oss<<"\t Adresse Postale : "<<endl;
    oss<<this->GetadrSiege().afficher();
    return oss.str();
}


Professionelle::~Professionelle()
{
    //dtor
    if (comments)
    {
        cout<<"The client professionelle "<<this->Getnom()<<" have been removed "<<endl;
    }
}
