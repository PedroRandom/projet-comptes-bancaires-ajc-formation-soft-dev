#include "dataclients.h"

DataClients::DataClients()
{
    //ctor
}

DataClients::~DataClients()
{
    //dtor
    if (comments)
    {
        cout<<"Destruction de la data des clients"<<endl;
    }
    for(auto i: this->GetlstClients())
    {
        if(comments){cout<<"Destruction du element"<<i.first<<endl;}
        delete lstClients.at(i.first);
    }
}

void DataClients::addClient(Client *val)
{
    if (this->nbClients()==0 || (this->GetlstClients()[val->Getidentifiant()]
                                ==nullptr))
    {
        this->lstClients.insert(std::pair<int,Client*>(val->Getidentifiant(),val));
    }
    else {
        throw InputException(ErrCode::ERR_CL_EXIST);
    }

}
