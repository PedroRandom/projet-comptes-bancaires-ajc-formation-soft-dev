#ifndef STRUCTSIMPLE_H_INCLUDED
#define STRUCTSIMPLE_H_INCLUDED

#include "InputException.h"

/*!
* \file StructSimple.h
* \brief Fichier dedie a des structures simples AdressePost et Dates
* \author Pedro Clavier
* \version 0.1
*/

/*! \class AdressPost
* \brief classe concrète AdressPost
*
*    Elle permet de gérer une adresse postale
*       Getter/Setter
*       afficher
*/
class AdressePost
{
public:

    AdressePost(string lib="A reemplir",string comp="",string vil=" A reemplir",int code=0);
    ~AdressePost(){
        if(comments){cout<<"Destruction d'adresse postale "<<endl;}}

    void setLibelle(string val){this->libelle=val;}
    string getLibelle(){return this->libelle;}

    void setComplement(string val){this->complement=val;}
    string getComplement(){return this->complement;}

    void setVille(string val){this->ville=val;}
    string getVille(){return this->ville;}

    /** \brief Setter du code postale , verifie qu'il est positive
    */
    void setCP(int cp);
    int getCP(){return this->cp;}

    /** \brief Permet de faire un setter de tous les params (gerer tout dans une ligne pour autres structures)
    *
    *
    */
    void setInit(string lib,string comp,string vil,int code);
    /** \brief Retour un string avec les informations du Adresse Postale
    *
    * \return string
    *
    */
    string afficher();

private:
    string libelle; /**< Libelle de l'adresse */
    string complement; /**< Complement de l'adresse */
    string ville;/**< Ville de l'adresse */
    int cp; /**< Code Postale de l'adresse */
};

/*! \class Dates
* \brief classe concrète Dates
*
*    Elle permet de gérer les infos lies a la date
*       Getter/Setter
*       afLine
*       afficher
*/
class Dates
{
public:
    Dates(int j=30,int m=2,int a=1000);
    ~Dates();
    int Getjour(){return jour;}
    int Getmois(){return mois;}
    int Getanne(){return anne;}
    void Setjour(int);
    void Setmois(int);
    void Setanne(int);
    /** \brief Permet de faire un setter de tous les params (gerer tout dans une ligne pour autres structures)
    *
    *
    */
    void setInit(int j,int m,int a);

    /** \brief Retour un string avec la date formate sans saute de ligne
    *
    * \return string
    *
    */
    string afLigne();
    /** \brief Retour un string avec la date formate avec saute de ligne
    *
    * \return string
    *
    */
    virtual string afficher();
private:
    int jour; /**< Journe de la date (Entre 1 et 31) */
    int mois; /**< Mois de la date (Entre 1 et 12) */
    int anne; /**< Anne de la date (Superieur a 1000) */

};


#endif // STRUCTSIMPLE_H_INCLUDED
