#include "StructSimple.h"
/** \brief Constructeur
 *
 *
 */
AdressePost::AdressePost(string lib, string comp, string vil, int code)
    :libelle(lib),complement(comp),ville(vil)
{
    this->setCP(code);
}
/** \brief Setter du code postale , verifie qu'il est positive
*/
void AdressePost::setCP(int cp)
{
    if(cp>=0)
    {
        this->cp=cp;
    }
    else
    {
        throw InputException(ErrCode::ERR_CP,"CP");
    }
}
/** \brief Permet de faire un setter de tous les params (gerer tout dans une ligne pour autres structures)
*
*
*/
void AdressePost::setInit(string lib, string comp, string vil, int code)
{
    this->setLibelle(lib);
    this->setComplement(comp);
    this->setVille(vil);
    this->setCP(code);
}

/** \brief Retour un string avec les informations du Adresse Postale
*
* \return string
*
*/
string AdressePost::afficher()
{
    ostringstream oss;
    oss<<"\t\t Libelle: "<<this->libelle<<endl;
    oss<< "\t\t Complement: "<<this->complement<<endl;
    oss<<"\t\t Code Postal: "<<this->cp<<endl;
    oss<<"\t\t Ville : "<<this->ville<<endl;
    return oss.str();
}
/** \brief Constructeur
 *
 *
 */
Dates::Dates(int j, int m, int a)
{
    this->setInit(j,m,a);
}
/** \brief Destructeur
 *
 *
 */
 /** \brief Permet de faire un setter de tous les params (gerer tout dans une ligne pour autres structures)
*
*
*/
void Dates::setInit(int j, int m, int a)
{
    this->Setjour(j);
    this->Setmois(m);
    this->Setanne(a);
}

Dates::~Dates()
{
    if (comments)
    {
        cout<<"The date have been removed "<<endl;
    }
}
/** \brief Mis a jour de la journee et verification de son valeur
 *
 *
 */
void Dates::Setjour(int j)
{
    if(j<=0||j>31)
    {
        throw InputException(ErrCode::ERR_JOUR);
    }
    else {this->jour=j;}
}
/** \brief Mis a jour du mois et verification de son valeur
 *
 *
 */
void Dates::Setmois(int m)
{
    if(m<=0||m>12)
    {
        throw InputException(ErrCode::ERR_MOIS);
    }
    else {this->mois=m;}
}
/** \brief Mis a jour de l'anee et verification de son valeur
 *
 *
 */
void Dates::Setanne(int a)
{
    if(a<1000)
    {
        throw InputException(ErrCode::ERR_ANNE);
    }
    else {this->anne=a;}
}
/** \brief Retour un string avec la date formate sans saute de ligne
*
* \return string
*
*/
string Dates::afLigne()
{
    ostringstream oss;
    oss<<setw(2)<<this->Getjour()<<"/"<<setw(2)<<this->Getmois()<<"/"<<this->Getanne();
    return oss.str();
}
/** \brief Retour un string avec la date formate avec saute de ligne
*
* \return string
*
*/
string Dates::afficher()
{
    ostringstream oss;
    oss<<this->afLigne();
    oss<<endl;
    return oss.str();
}
