#ifndef PARTICULAIRE_H
#define PARTICULAIRE_H

#include "Client.h"
/*!
* \file Particulaire.h
* \brief Fichier dedie a la structure concrete Particulaire
* \author Pedro Clavier
* \version 0.3
*/

/*! \class Particulaire
* \brief classe dedie a les infos specifiques des clients particulaires
*
*    Elle permet de g?rer les infos specifiques des clients particulaires
*       Getter/Setters
*       toString
*       afficher
*/

class Particulaire : public Client
{
    public:
        /** Default constructor */
        Particulaire(int ident=0,string n="A remplir",string lib="A reemplir",string comp="",string vil=" A reemplir",int code=0,string mail="aremplir@inconue.com",
                     int j=30,int m=2,int a=1000,string pren="A remplir",char sex='M');
        /** Default destructor */
        virtual ~Particulaire();
        /** Return the string for age line in interface graphique
         * \return The age and "Bon anniversaire" if it is the birthday
         */
        string GetAfficheAnniv();
        /** Access naissance
         * \return The current value of naissance
         */
        Dates Getnaissance() { return naissance; }
        /** Set naissance
         * \param val New value to set
         */
        void Setnaissance(Dates val) { naissance = val; }
        /** Access prenom
         * \return The current value of prenom
         */
        string Getprenom() { return prenom; }
        /** Set prenom
         * \param Nouvelle value pour le prenom, s'assure que sont longueur soit inferieur a celle defini
         */
        void Setprenom(string val);
        /** Access sexe
         * \return The current value of sexe
         */
        char Getsexe() { return sexe; }
        /** Set sexe
         * \param Nouvelle value pour le sexe, s'assure qu'il soit 'F/M'
         */
        void Setsexe(char val);
        /** \brief Method utilis� pour l'affichage dans l'ecran (Seuelement une part est affiche) avec polymorphisme
        *
        * \return string
        *
        */
        virtual string toStringScreen() override;

        /** \brief Method utilis� pour le polymorphisme, ajout les infos generales et les specifiques (celles du afficher)
        *
        * \return string
        *
        */
        virtual string toString() override;
        /** \brief Retour un string avec les informations specifiques du client particulaire
        *
        * \return string
        *
        */
        virtual string afficher() override;

    protected:

    private:
        Dates naissance; //!< Date de naissance du client
        string prenom; //!< Prenom du client
        char sexe; //!< Sexe du client(M/F)
};

#endif // PARTICULAIRE_H
