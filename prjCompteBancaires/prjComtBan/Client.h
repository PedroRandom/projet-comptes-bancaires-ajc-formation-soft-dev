#ifndef CLIENT_H
#define CLIENT_H
#include "InputException.h"
#include "StructSimple.h"

/*!
* \file Client.h
* \brief Fichier dedie a la structure abstraite Client qui sert de basser a Particulaires et Professionels
* \author Pedro Clavier
* \version 0.2
*/

/*! \class Client
* \brief classe dedie a les infos communs de tous les clients
*
*    Elle permet de g�rer les infos basse de tous les clients
*       Getter/Setters
*       toString
*       afficher
*/
class Client
{
    public:
        Client(int ident=0,string n="A remplir",string lib="A reemplir",string comp="",string vil=" A reemplir",int code=0,string mail="aremplir@inconue.com");
        virtual ~Client();

        int Getidentifiant() { return identifiant; }
        void Setidentifiant(int val) { identifiant = val; }
        string Getnom() { return nom; }
        void Setnom(string val);
        AdressePost GetadPost() { return adPost; }
        void SetadPost(AdressePost val) { adPost = val; }
        string Getmail() { return mail; }
        void Setmail(string val);
        /** \brief Method utilis� pour creer des tabulations pour l'affichage dans l'ecran
        *
        * \return ostringstream
        *
        */
        string tab()
        {
            ostringstream oss;
            oss<<setfill(' ')<<setw(tabLong)<<" ";
            return oss.str();
        }
        /** \brief Method abstrait utilis� pour l'affichage dans l'ecran (Seuelement une part est affiche)
        *
        * \return string
        *
        */
        virtual string toStringScreen()=0;
        /** \brief Method abstrait, sera complete avec les informations specifiques du client (Particulaire/Professionelle) et celles du afficher
        *
        * \return string
        *
        */
        virtual string toString()=0;
        /** \brief Retour un string avec les informations generals du client
        *
        * \return string
        *
        */
        virtual string afficher();
        /** \brief Method utilis� pour l'affichage dans l'ecran de l'adress
        *
        * \return string
        *
        */
        string toStrAdressScreen();
    protected:

    private:
        int identifiant; /**< Identifiant du cleint */
        string nom; /**< Nom du client, max 50 chars */
        AdressePost adPost; /**< Adresse Postale du client */
        string mail; /**< Mail du client */
};

#endif // CLIENT_H
