#ifndef INPUTEXCEPTION_H
#define INPUTEXCEPTION_H
#include <exception>
#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>
#include <algorithm>
#include <map>
#define comments 0
#define charLong 50
#define sirLong 14
#define tabLong 12
#define anomFilename "Anomalies.log"
using namespace std;

/*!
* \file InputException.h
* \brief Fichier dedie a la structure utilis� pour la gestion des erreurs
* \author Pedro Clavier
* \version 0.1
*/

/*! \class ErrCode
* \brief Enumeration avec les diferents erreurs geres
*
*/
enum class ErrCode{ERR_CP,ERR_JOUR,ERR_MOIS,ERR_ANNE,ERR_MAIL,ERR_CHAINE,ERR_SEX,ERR_NUM_DET, ERR_ST_JUR, ERR_MAX,ERR_CL_EXIST,ERR_DB,ERR_FIC};

/*! \class InputException
* \brief classe dedie a gerer les exceptions
*
*    Elle permet de g�rer les exceptions
*       what
*/
class InputException : public exception
{
    public:
        InputException(ErrCode cod,string text="numero",int maxi=0);
        virtual ~InputException();

        /** \brief Retour un string avec les informations de l'erreur
        *
        * \return string
        *
        */
        virtual const char* what() const throw() override;

    protected:

    private:
        ErrCode code; /**< Code de l'erreur produit */
        string text; /**< Text que complement l'erreur produit (P.Ej: 'CP' ou 'Montant' quand il y a un erreur de num positif) */
        mutable string expErr; /**< Explication de l'erreur produit */
        int maxi; /**< Quantit� que complement un erreur produit li� a un numero maxim */
};

#endif // INPUTEXCEPTION_H
